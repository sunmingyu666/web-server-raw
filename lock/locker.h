#ifndef LOCKER_H
#define LOCKER_H

// 该头文件实现了锁机制，保证了多线程同步，任一时刻只能有一个线程进入关键代码段

/*
    实现方式：RAII思想——资源获取即初始化
    C++的语言机制保证了当一个对象创建的时候，自动调用构造函数，当对象超出其作用域的时候
    则自动调用析构函数，释放其资源。
    因此，以类的方式来管理资源，将资源与对象的生命周期绑定起来，活用C++的语言机制，会更
    加智能与安全——比如智能指针
*/
#include <exception>
#include <pthread.h>
#include <semaphore.h>

/*
    信号量：
    特殊变量，只能取自然数，只支持wait和post操作；
    wait:如果信号量的值>0，将其-一，否则将执行的线程挂起；
    post：如果有因为等待信号量而挂起的线程，将其唤醒，否则将信号量+1；
*/
class sem
{
public:
    sem()
    {
        if (sem_init(&m_sem, 0, 0) != 0)
        {
            throw std::exception();
        }
    }
    sem(int num)
    {
        if (sem_init(&m_sem, 0, num) != 0)
        {
            throw std::exception();
        }
    }
    ~sem()
    {
        sem_destroy(&m_sem);
    }
    bool wait()
    {
        return sem_wait(&m_sem) == 0;
    }
    bool post()
    {
        return sem_post(&m_sem) == 0;
    }

private:
    sem_t m_sem;
};

/*
    互斥锁：
    保证独占式访问关键代码段，保证线程同步的重要手段，进入关键代码段时，以原子操作对该
    代码段加锁，离开时唤醒等待互斥锁的线程
*/

class locker
{
public:
    locker()
    {
        if (pthread_mutex_init(&m_mutex, NULL) != 0)
        {
            throw std::exception();
        }
    }
    ~locker()
    {
        pthread_mutex_destroy(&m_mutex);
    }
    bool lock()
    {
        return pthread_mutex_lock(&m_mutex) == 0;
    }
    bool unlock()
    {
        return pthread_mutex_unlock(&m_mutex) == 0;
    }
    pthread_mutex_t *get()
    {
        return &m_mutex;
    }

private:
    pthread_mutex_t m_mutex;
};

/*
    条件变量：
    提供了线程之间的一种通知机制，当某个共享数据达到某个值的时候，唤醒等待这个共享数据的
    线程，一般用以避免线程之间无效竞争，导致CPU空转
*/

class cond
{
public:
    cond()
    {
        if (pthread_cond_init(&m_cond, NULL) != 0)
        {
            //pthread_mutex_destroy(&m_mutex);
            throw std::exception();
        }
    }
    ~cond()
    {
        pthread_cond_destroy(&m_cond);
    }
    bool wait(pthread_mutex_t *m_mutex)
    {
        int ret = 0;
        // pthread_mutex_lock(&m_mutex);
        // 等待目标条件变量，调用时需要传入已经加锁的互斥锁，先把调用线程放入条件变量
        // 的请求队列,然后将互斥锁mutex解锁，当函数成功返回为0时，互斥锁会再次被锁上
        ret = pthread_cond_wait(&m_cond, m_mutex);
        // pthread_mutex_unlock(&m_mutex);
        return ret == 0;
    }
    bool timewait(pthread_mutex_t *m_mutex, struct timespec t)
    {
        int ret = 0;
        // pthread_mutex_lock(&m_mutex);
        ret = pthread_cond_timedwait(&m_cond, m_mutex, &t);
        // pthread_mutex_unlock(&m_mutex);
        return ret == 0;
    }
    bool signal()
    {
        return pthread_cond_signal(&m_cond) == 0;
    }
    bool broadcast()
    {
        return pthread_cond_broadcast(&m_cond) == 0;
    }

private:
    // static pthread_mutex_t m_mutex;
    pthread_cond_t m_cond;
};
#endif
