#ifndef HTTPCONNECTION_H
#define HTTPCONNECTION_H
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/epoll.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <assert.h>
#include <sys/stat.h>
#include <string.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <stdarg.h>
#include <errno.h>
#include <sys/wait.h>
#include <sys/uio.h>
#include <map>

#include "../lock/locker.h"
#include "../CGImysql/sql_connection_pool.h"
#include "../timer/lst_timer.h"
#include "../log/log.h"

/*
    epoll IO多路复用
    #include <sys/epoll.h>
    1、int epoll_create(int size)
    返回一个指向epoll内核事件表的文件描述符，作为其他epoll系统调用的第一个参数，size不起作用

    2、int epoll_ctl(int epfd, int op, int fd, struct epoll_event *event)
    用于操作内核事件表监控的文件描述符上的事件：注册、修改、删除
    
    op：表示动作，用3个宏来表示：
        EPOLL_CTL_ADD (注册新的fd到epfd)，
        EPOLL_CTL_MOD (修改已经注册的fd的监听事件)，
        EPOLL_CTL_DEL (从epfd删除一个fd)；
    
    event：告诉内核需要监听的事件
    
    event是epoll_event结构体指针类型，表示内核所监听的事件，具体定义如下：
        struct epoll_event {
        __uint32_t events; Epoll events 
        epoll_data_t data; User data variable 
        };

    events描述事件类型，其中epoll事件类型有以下几种
        EPOLLIN：表示对应的文件描述符可以读（包括对端SOCKET正常关闭）
        EPOLLOUT：表示对应的文件描述符可以写
        EPOLLPRI：表示对应的文件描述符有紧急的数据可读（这里应该表示有带外数据到来）
        EPOLLERR：表示对应的文件描述符发生错误
        EPOLLHUP：表示对应的文件描述符被挂断
        EPOLLET：将EPOLL设为边缘触发(Edge Triggered)模式，这是相对于水平触发(Level Triggered)而言的
        EPOLLONESHOT：只监听一次事件，当监听完这次事件之后，如果还需要继续监听这个socket的话，需要再次把这个socket加入到EPOLL队列里

    3、int epoll_wait(int epfd, struct epoll_event *events, int maxevents, int timeout)
    用于等待所监听的文件描述符上有事件的产生，返回事件就绪的文件描述符的个数
    events：用来存内核得到事件的集合
    maxevents：告之内核这个events有多大，这个maxevents的值不能大于创建epoll_create()时的size，
    timeout：是超时时间
        -1：永久阻塞
        0：立即返回，非阻塞
        >0：指定阻塞时间
*/

/*
    HTTP报文处理流程
    1、浏览器端发出http连接请求，主线程创建http对象接收请求并将所有数据读入对应buffer，将该对象插入任务队列，工作线程从任务队列中取出
    一个任务进行处理。
    2、工作线程取出任务后，调用process_read函数，通过主、从状态机对请求报文进行解析。
    3、解析完之后，跳转do_request函数生成响应报文，通过process_write写入buffer，返回给浏览器端。
*/

class http_conn
{
public:
    // 设置读取文件名称 m_real_file 大小
    static const int FILENAME_LEN = 200;
    // 设置读缓冲区 m_read_buffer 大小
    static const int READ_BUFFER_SIZE = 2048;
    // 设置写缓冲区 m_write_buffer 大小
    static const int WRITE_BUFFER_SIZE = 1024;
    // 报文的请求方法，本项目只用到GET和POST
    enum METHOD
    {
        GET = 0,
        POST,
        HEAD,
        PUT,
        DELETE,
        TRACE,
        OPTIONS,
        CONNECT,
        PATH
    };
    // 主状态机的状态
    enum CHECK_STATE
    {
        CHECK_STATE_REQUESTLINE = 0,
        CHECK_STATE_HEADER,
        CHECK_STATE_CONTENT
    };
    // 报文解析的结果
    enum HTTP_CODE
    {
        NO_REQUEST,
        GET_REQUEST,
        BAD_REQUEST,
        NO_RESOURCE,
        FORBIDDEN_REQUEST,
        FILE_REQUEST,
        INTERNAL_ERROR,
        CLOSED_CONNECTION
    };
    // 从状态机的状态
    enum LINE_STATUS
    {
        LINE_OK = 0,
        LINE_BAD,
        LINE_OPEN
    };

public:
    http_conn() {}
    ~http_conn() {}

public:
    // 初始化套接字地址，函数内部会调用私有方法init
    // 其他参数？
    void init(int sockfd, const sockaddr_in &addr, char *, int, int, string user, string passwd, string sqlname);
    // 关闭http连接
    void close_conn(bool real_close = true);
    void process();
    // 一次性读取浏览器端发来的全部数据
    bool read_once();
    // 写入响应报文
    bool write();
    sockaddr_in *get_address()
    {
        return &m_address;
    }
    // 同步线程初始化数据库读取表
    void initmysql_result(connection_pool *connPool);
    // 这两个参数是否与定时器有关？
    int timer_flag;
    int improv;

private:
    void init();
    // 从m_read_buf读取，并处理请求报文
    HTTP_CODE process_read();
    // 向m_write_buf写入响应报文数据
    bool process_write(HTTP_CODE ret);
    // 主状态机解析报文中的请求行数据
    HTTP_CODE parse_request_line(char *text);
    // 主状态机解析报文中的请求头数据
    HTTP_CODE parse_headers(char *text);
    // 主状态机解析报文中的请求内容
    HTTP_CODE parse_content(char *text);
    // 生成响应报文
    HTTP_CODE do_request();
    
    // m_start_line是已经解析的字符
    // get_line用于将指针向后偏移，指向未处理的字符
    char *get_line() { return m_read_buf + m_start_line; };
    
    // 从状态机读取一行，分析是请求报文的哪一部分
    LINE_STATUS parse_line();
    
    void unmap();

    // 根据响应报文格式，生成对应8个部分，以下函数均由do_request调用
    bool add_response(const char *format, ...);
    bool add_content(const char *content);
    bool add_status_line(int status, const char *title);
    bool add_headers(int content_length);
    bool add_content_type();
    bool add_content_length(int content_length);
    bool add_linger();
    bool add_blank_line();

public:
    static int m_epollfd;
    static int m_user_count;
    MYSQL *mysql;
    int m_state;  //读为0, 写为1

private:
    int m_sockfd;
    sockaddr_in m_address;

    // 存储读取的请求报文数据
    char m_read_buf[READ_BUFFER_SIZE];
    // 缓冲区中m_read_buf中数据的最后一个字节的下一个位置
    long m_read_idx;
    // m_read_buf读取的位置m_checked_idx
    long m_checked_idx;
    // m_read_buf中已经解析的字符个数
    int m_start_line;
    
    // 存储发出的响应报文数据
    char m_write_buf[WRITE_BUFFER_SIZE];
    // 指示buffer中的长度
    int m_write_idx;

    // 主状态机的状态
    CHECK_STATE m_check_state;
    // 请求方法
    METHOD m_method;

    // 以下为解析请求报文中对应的6个变量
    // 存储读取文件的名称
    char m_real_file[FILENAME_LEN];
    char *m_url; // 统一资源定位符
    char *m_version; // HTTP版本号
    char *m_host; // 代表服务器域名
    long m_content_length; // 内容主体的大小
    bool m_linger; // 代表长连接

    // 读取服务器上的文件地址
    char *m_file_address;
    struct stat m_file_stat;
    // io向量机制iovec
    struct iovec m_iv[2];
    int m_iv_count;
    // 是否启用的POST
    int cgi;
    // 存储请求头数据 
    char *m_string;
    int bytes_to_send;
    int bytes_have_send;
    char *doc_root;

    // 以下成员变量含义？
    map<string, string> m_users;
    // epoll工作模式，0代表水平触发LT，1代表边缘触发ET
    int m_TRIGMode;
    int m_close_log;

    char sql_user[100];
    char sql_passwd[100];
    char sql_name[100];
};

#endif
