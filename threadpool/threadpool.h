#ifndef THREADPOOL_H
#define THREADPOOL_H

#include <list>
#include <cstdio>
#include <exception>
#include <pthread.h>
#include "../lock/locker.h"
#include "../CGImysql/sql_connection_pool.h"

/*
    1、静态成员变量
    在编译阶段，对象还未创建时就已经分配了空间，且静态成员变量属于一个类，所有对象都共享；
    最好是类内声明，类外初始化（非静态成员不能类外初始化）；
    静态成员数据是共享的；
*/

/*
    2、静态成员函数
    静态成员函数可以直接访问静态成员变量，但不能访问普通成员变量，相反普通成员函数既可以访问静态成员变量，也可以访问普通成员变量；
    静态成员函数为共享函数，没有this指针；
*/

/*
    3、半同步/半反应堆线程池
    反应堆具体为Proactor事件处理模式：主线程作为异步线程，负责监听文件描述符，接收socket新连接，若当前监听到的socket发生了读写事件
    就将任务插入到请求队列中。并唤醒其中一个工作线程，完成对读写数据的处理。
*/

template <typename T>
class threadpool
{
public:
    /*thread_number是线程池中线程的数量，max_requests是请求队列中最多允许的、等待处理的请求的数量*/
    // connPool是数据库连接池指针
    threadpool(int actor_model, connection_pool *connPool, int thread_number = 8, int max_request = 10000);
    ~threadpool();
    bool append(T *request, int state);
    bool append_p(T *request);

private:
    /*工作线程运行的函数，它不断从工作队列中取出任务并执行之*/
    static void *worker(void *arg);
    void run();

    // 成员变量中没有结束线程标记
private:
    int m_thread_number;        //线程池中的线程数
    int m_max_requests;         //请求队列中允许的最大请求数
    pthread_t *m_threads;       //描述线程池的数组，其大小为m_thread_number
    std::list<T *> m_workqueue; //请求队列
    locker m_queuelocker;       //保护请求队列的互斥锁
    sem m_queuestat;            //是否有任务需要处理
    connection_pool *m_connPool;//数据库连接池？
    int m_actor_model;          //模型切换？
};

template <typename T>
threadpool<T>::threadpool( int actor_model, connection_pool *connPool, int thread_number, int max_requests) : m_actor_model(actor_model),m_thread_number(thread_number), m_max_requests(max_requests), m_threads(NULL),m_connPool(connPool)
{
    if (thread_number <= 0 || max_requests <= 0)
        throw std::exception();
    
    // 线程id初始化
    m_threads = new pthread_t[m_thread_number];
    if (!m_threads)
        throw std::exception();
    
    for (int i = 0; i < thread_number; ++i)
    {
        //循环创建线程，并将工作线程按要求进行运行
        // 传入this指针，以便静态成员函数可以访问普通成员变量，并在静态函数中引用这个对象，调用其动态方法run
        if (pthread_create(m_threads + i, NULL, worker, this) != 0)
        {
            delete[] m_threads;
            throw std::exception();
        }
        //将线程进行分离后，不用单独对工作线程进行回收
        if (pthread_detach(m_threads[i]))
        {
            delete[] m_threads;
            throw std::exception();
        }
    }
}

template <typename T>
threadpool<T>::~threadpool()
{
    delete[] m_threads;
}

template <typename T>
bool threadpool<T>::append(T *request, int state)
{
    // 操作工作队列时一定要加锁，因为它被所有线程共享。
    m_queuelocker.lock();
    // 根据硬件，预先设置请求队列的最大值
    if (m_workqueue.size() >= m_max_requests)
    {
        m_queuelocker.unlock();
        return false;
    }
    // 传入HTTP解析任务类型，其中m_state读为0, 写为1
    request->m_state = state;
    m_workqueue.push_back(request);
    m_queuelocker.unlock();
    // 信号量/待处理任务数+1
    m_queuestat.post();
    return true;
}

template <typename T>
bool threadpool<T>::append_p(T *request)
{
    m_queuelocker.lock();
    if (m_workqueue.size() >= m_max_requests)
    {
        m_queuelocker.unlock();
        return false;
    }
    m_workqueue.push_back(request);
    m_queuelocker.unlock();
    m_queuestat.post();
    return true;
}

template <typename T>
void *threadpool<T>::worker(void *arg)
{
    // 将参数强转为线程池类，调用成员方法
    threadpool *pool = (threadpool *)arg;
    // 线程池创建出线程后需要运行——run函数
    pool->run();
    return pool;
}

template <typename T>
void threadpool<T>::run()
{
    while (true)
    {
        // 信号量等待：工作队列中没有任务则阻塞，其中优先级最高的子线程获得信号量，其他子线程仍旧阻塞在wait处
        m_queuestat.wait();
        // 被唤醒后先加互斥锁
        m_queuelocker.lock();
        if (m_workqueue.empty())
        {
            m_queuelocker.unlock();
            continue;
        }
        T *request = m_workqueue.front();
        m_workqueue.pop_front();
        m_queuelocker.unlock();
        // 没有获取到则重新循环——加上更加安全些，不加也没问题
        if (!request)
            continue;
        if (1 == m_actor_model)
        {
            if (0 == request->m_state)
            {
                //循环读取客户数据，直到无数据可读或对方关闭连接
                //非阻塞ET工作模式下，需要一次性将数据读完
                if (request->read_once())
                {
                    // improv与timer_flag分别代表？
                    request->improv = 1;
                    // 从连接池中取出一个数据库连接？
                    connectionRAII mysqlcon(&request->mysql, m_connPool);
                    // process（模板类中的方法,这里是http类）进行处理
                    // 执行任务（工作），这里不用锁，并发执行
                    request->process();
                }
                else
                {
                    request->improv = 1;
                    request->timer_flag = 1;
                }
            }
            else
            {
                if (request->write())
                {
                    request->improv = 1;
                }
                else
                {
                    request->improv = 1;
                    request->timer_flag = 1;
                }
            }
        }
        else
        {
            connectionRAII mysqlcon(&request->mysql, m_connPool);
            request->process();
        }
    }
}
#endif
